/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.


  El objetivo del TaskManager es comunicarse con los procesos y transmitirles órdenes.
  Cuando el motor de la orden de actualizarse, transmitirá esa orden al TaskManager
*/

#include "rf_taskmanager.h"
#include "rf_engine.h"

RF_TaskManager* RF_TaskManager::instance {nullptr};

RF_TaskManager::RF_TaskManager()
{
    if(instance == nullptr) instance = this;
    else delete this;
}
RF_TaskManager::~RF_TaskManager()
{
  //Borrar árbol
    Clear();

  instance = nullptr;
}

bool RF_TaskManager::existsTask(std::string id)
{
  return isInList(id);
}

RF_Process* RF_TaskManager::getTask(std::string id)
{
    return Get(id);
}

std::vector<RF_Process*> RF_TaskManager::getTaskByType(std::string type)
{
    std::pair<std::unordered_multimap<std::string, RF_Process*>::iterator,std::unordered_multimap<std::string, RF_Process*>::iterator> par = typeMap.equal_range(type);
    std::vector<RF_Process*> ret;
    for(auto& it = par.first; it != par.second; ++it)
    {
      if(it->second != nullptr)
      {
          ret.push_back(it->second);
      }
    }
    return ret;
}

std::vector<RF_Process*> RF_TaskManager::getTaskByFather(std::string father)
{
  std::vector<RF_Process*> ret;
  std::vector<std::string> v;
  split(father,':',v);

  Node *it = root;
  Node *aux;
  for (long unsigned int i = 1; i < v.size(); ++i)
  {
      aux = it->firstChild;
      while (aux != NULL && aux->key != v[i])
      {
          aux = aux->son;
      }

      if (aux != NULL)
      {
        if(aux->data != nullptr && aux->data->id == father)
        {
          Node *son = aux->firstChild;
          while(son != nullptr)
          {
            ret.push_back(son->data);
            son = son->son;
          }
          return ret;
        }
        else
        {
          it = aux;
        }
      }
  }

  return ret;
}

std::vector<RF_Process*> RF_TaskManager::getTaskList()
{
  std::vector<RF_Process*> ret;
  for(auto& it : taskMap)
  {
    if(it.second != nullptr)
    {
      ret.push_back(it.second);
    }
  }

  return ret;
}

std::vector<std::string> RF_TaskManager::getTaskIdList()
{
  std::vector<std::string> ret;
  for(auto& it : taskMap)
  {
    if(it.second != nullptr)
    {
      ret.push_back(it.first);
    }
  }

  return ret;
}

void RF_TaskManager::deleteTask(std::string id)
{
  RemoveAll(id);
}
void RF_TaskManager::deleteTask(RF_Process* task)
{
  RemoveAll(task->id);
}

void RF_TaskManager::Update()
{
  manageSignals();

  for(auto& it : taskMap)
  {
    if(it.second != nullptr && it.second->signal == RF_Structs::S_AWAKE)
    {
      it.second->Update();
    }
  }
}

void RF_TaskManager::FixedUpdate()
{
  for(auto& it : taskMap)
  {
    if(it.second != nullptr && it.second->signal == RF_Structs::S_AWAKE)
    {
      it.second->FixedUpdate();
    }
  }
}

void RF_TaskManager::Draw()
{
  for(auto e = taskMap.begin(); e != taskMap.end(); ++e)
  {
    if(e->second != nullptr)
    {
      e->second->Draw();
      if(e->second->graph != NULL && e->second->window != nullptr)
      {
          std::vector<RF_Process*>::iterator it = toRend.begin(), first = toRend.begin(), last = toRend.end();
          std::iterator_traits<std::vector<RF_Process*>::iterator>::difference_type count, step;
          count = std::distance(first, last);

          while(count > 0)
          {
            it = first; step=count/2; std::advance (it,step);
            if (!(e->second->zLayer<(*it)->zLayer))
            {
              first=++it; count-=step+1;
            }
            else
            {
              count=step;
            }
          }
          if(e->second->signal != RF_Structs::S_SLEEP && e->second->signal != RF_Structs::S_SLEEP_TREE)
          {
            toRend.insert(it, e->second);
          }
      }
    }
  }

  for(auto it : toRend)
  {
    it->LateDraw();
    it->window->Rend(it->graph, &it->transform, it->flipType);
    it->AfterDraw();
  }

  toRend.clear();
}

void RF_TaskManager::Clear()
{
  RemoveAll();
}

void RF_TaskManager::manageSignals()
{
	//#pragma omp parallel for
	for(std::string pid : getTaskIdList())
	{
		if(existsTask(pid))
		{
			RF_Process* it = getTask(pid);
			if(it != nullptr)
			{
				switch(it->signal)
				{
					case RF_Structs::S_AWAKE_TREE:
						for(RF_Process* task : getTaskByFather(it->id))
						{
							sendSignal(task, RF_Structs::S_AWAKE_TREE);
						}
						it->signal = RF_Structs::S_AWAKE;
						break;

					case RF_Structs::S_KILL:
						deleteTask(it);
						break;

					case RF_Structs::S_KILL_TREE:
						deleteTask(it->id);
						break;

					case RF_Structs::S_KILL_CHILD:
						for(RF_Process* task : getTaskByFather(it->id))
						{
							sendSignal(task->id, RF_Structs::S_KILL_TREE);
						}

						it->signal = RF_Structs::S_AWAKE;
						break;

					case RF_Structs::S_SLEEP_TREE:
						for(RF_Process* task : getTaskByFather(it->id))
						{
							sendSignal(task, RF_Structs::S_SLEEP_TREE);
						}
						it->signal = RF_Structs::S_SLEEP;
						break;
				}
			}
		}
	}
}
void RF_TaskManager::sendSignal(std::string taskID, int signal)
{
	sendSignal(getTask(taskID), signal);
}
void RF_TaskManager::sendSignal(RF_Process* task, int signal)
{
	if(task != nullptr)
	{
		task->signal = signal;
	}
}
