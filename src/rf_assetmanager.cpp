/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "rf_assetmanager.h"

std::unordered_map<std::string, RF_AssetList*> RF_AssetManager::Private::packages;

void RF_AssetManager::loadAssetPackage(std::string package)
{
  const size_t it = package.find_last_of('/');
  std::string id = package; id.erase(0,it+1);

  if(!RF_AssetManager::isLoaded(id))
  {
    RF_AssetList* aL = new RF_AssetList(package);
    Private::packages[aL->id] = aL;
  }
}

void RF_AssetManager::unloadAssetPackage(std::string package)
{
  if(RF_AssetManager::isLoaded(package))
  {
    Private::packages.erase(package);
  }
}

int RF_AssetManager::Size()
{
  return Private::packages.size();
}

int RF_AssetManager::packageSize(std::string package)
{
  return Private::packages[package]->Size();
}

bool RF_AssetManager::isLoaded(std::string package, std::string id)
{
  bool ret = (Private::packages[package] != nullptr);
  if(ret == true && id != "")
  {
    ret = Private::packages[package]->isLoaded(id);
  }

  return ret;
}

void RF_AssetManager::addResource(std::string extension, LoadFunc function)
{
  RF_AssetList::addResource(extension, function);
}
