/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "rf_map.h"

void RF_Map::Add(std::string key, RF_Process *dat)
{
  std::vector<std::string> v;
  split(key,':',v);

  Node *it = root;
  Node *aux;
  for (long unsigned int i = 1; i < v.size(); ++i)
  {
    aux = it->firstChild;
    while (aux != nullptr && aux->key != v[i])
    {
      aux = aux->son;
    }

    if (aux != NULL && aux->key != "")
    {
      it = aux;
    }
    else
    {
      it->addChild(v[v.size()-1], dat);
      taskMap[key] = dat;
      typeMap.insert(std::pair<std::string, RF_Process*>(dat->type, dat));
      return;
    }
  }
}

bool RF_Map::isInList(std::string key)
{
  return (Get(key) != nullptr);
}

RF_Process* RF_Map::Get(std::string key)
{
  return taskMap[key];
}

void RF_Map::Remove(std::string key)
{
    std::vector<std::string> v;
    split(key,':',v);

    Node *it = root;
    Node *aux;
    for (long unsigned int i = 1; i < v.size(); ++i)
    {
        aux = it->firstChild;
        while (aux != NULL && aux->key != v[i])
        {
            aux = aux->son;
        }

        if (aux != NULL)
        {
          if(aux->key != v[v.size()-1])
          {
            it = aux;
          }
          else
          {
            std::pair<std::unordered_multimap<std::string, RF_Process*>::iterator,std::unordered_multimap<std::string, RF_Process*>::iterator> par = typeMap.equal_range(taskMap[key]->type);
            for(auto it = par.first; it != par.second; ++it)
            {
              if(it->second != nullptr && it->second->id == key)
              {
                typeMap.erase(it);
                break;
              }
            }
            taskMap.erase(key);

            if(it != nullptr)
            {
              if(aux->data->id == it->firstChild->data->id)
              {
                it->firstChild = aux->son;
              }
              else
              {
                auto itt = it->firstChild;
                for(; aux->data->id != itt->son->data->id; itt = itt->son);
                itt->son = aux->son;
              }

            }

            aux->data->Dispose();
            delete aux->data;

            aux->key = "";
            aux->data = nullptr;
            aux->firstChild = nullptr;
          }
        }
    }
}

void RF_Map::RemoveAll(std::string key)
{
  std::vector<std::string> v;
  split(key,':',v);

  Node *it = root;
  Node *aux;
  for (long unsigned int i = 1; i < v.size(); ++i)
  {
    aux = it->firstChild;
    while (aux != nullptr && aux->key != v[i])
    {
      aux = aux->son;
    }
    if (aux != nullptr)
    {
      if(i < v.size() - 1)
      {
        it = aux;
      }
      else
      {
        _Remove(aux, it);
      }
    }
  }
}
void RF_Map::_Remove(Node *aux, Node *father)
{
  if(aux == nullptr){return;}

  if(aux->firstChild != nullptr)
  {
    Node *i = aux->firstChild;
    Node *next;
    while(i != nullptr)
    {
      next = i->son;
      _Remove(i);
      i = next;
    }
  }

  if(aux->key != "")
  {
    std::pair<std::unordered_multimap<std::string, RF_Process*>::iterator,std::unordered_multimap<std::string, RF_Process*>::iterator> par = typeMap.equal_range(taskMap[aux->data->id]->type);
    for(auto it = par.first; it != par.second; ++it)
    {
      if(it->second != nullptr && it->second->id == aux->data->id)
      {
        typeMap.erase(it);
        break;
      }
    }
    taskMap.erase(aux->data->id);

    if(father != nullptr)
    {
      if(aux->data->id == father->firstChild->data->id)
      {
        father->firstChild = aux->son;
      }
      else
      {
        auto it = father->firstChild;
        for(; aux->data->id != it->son->data->id; it = it->son);
        it->son = aux->son;
      }

    }

    aux->data->Dispose();
    delete aux->data;

    aux->key = "";
    aux->firstChild = nullptr;
    delete aux;
  }
}

void RF_Map::RemoveAll()
{
  _Remove(root);

  taskMap.clear();
  typeMap.clear();
}

void RF_Map::split(const std::string& s, char delim,std::vector<std::string>& v)
{
    int i = 0;
    long unsigned int pos = s.find(delim);
    while (pos != std::string::npos)
    {
        v.push_back(s.substr(i, pos-i));
        i = ++pos;
        pos = s.find(delim, pos);

        if (pos == std::string::npos)
        {
            v.push_back(s.substr(i, s.length()));
        }
    }
}
