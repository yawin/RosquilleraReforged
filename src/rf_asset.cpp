/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "rf_asset.h"

RF_AudioClip::RF_AudioClip(std::string name, Mix_Music* song):RF_Asset(name), clip(song){}
RF_AudioClip::~RF_AudioClip()
{
  Mix_FreeMusic(clip);
  clip = nullptr;
}
void* RF_AudioClip::GetSource()
{
  return clip;
}

RF_FXClip::RF_FXClip(std::string name, Mix_Chunk* fx):RF_Asset(name), clip(fx){}
RF_FXClip::~RF_FXClip()
{
  Mix_FreeChunk(clip);
  clip = nullptr;
}
void* RF_FXClip::GetSource()
{
  return clip;
}

RF_Gfx2D::RF_Gfx2D(std::string name, SDL_Surface* gfx):RF_Asset(name), surface(gfx){}
RF_Gfx2D::~RF_Gfx2D()
{
  SDL_FreeSurface(surface);
  surface = nullptr;
}
void* RF_Gfx2D::GetSource()
{
  return surface;
}

RF_Font::RF_Font(std::string name, TTF_Font* ttf, std::string _path):RF_Asset(name), font(ttf), path(_path){}
RF_Font::~RF_Font()
{
  TTF_CloseFont(font);
  font = nullptr;
}
void* RF_Font::GetSource()
{
  return font;
}


RF_AssetList::RF_AssetList(std::string path)
{
  ///Por el momento, path es un directorio
    //Obtenemos el id del paquete de recursos
        const size_t it = path.find_last_of('/');
        id = path; id.erase(0,it+1);

    //Extraemos el nombre del directorio para insertarlo en el nombre;
        DIR *dir;

    //En *ent habrá información sobre el archivo que se está "sacando" a cada momento;
        struct dirent *ent;

    //Empezaremos a leer en el directorio actual;
        dir = opendir (path.c_str());

    //Miramos que no haya error
        if (dir == NULL)
        {
            RF_Engine::Debug(("LoadAsset [Error]: No se puede abrir directorio " + path));
            return;
        }

    //Abrimos el archivo de configuración
        Parser* parser = new Parser(path + "/package.json");
        if(parser->loaded)
        {
          RF_Engine::Debug((path + "/package.json"));
        }

    //Leyendo uno a uno todos los archivos que hay
        RF_Asset* asset = nullptr;
        while ((ent = readdir (dir)) != NULL)
        {
            //Los ficheros "." y ".." son punteros al directorio actual y al directamente superior
                if((strcmp(ent->d_name, ".")!=0) && (strcmp(ent->d_name, "..")!=0))
                {
                    std::string ext = ent->d_name;
                    const size_t pos = ext.find_last_of('.');
                    std::string Aid = ext; Aid.erase(pos);
                    ext.erase(0,pos+1);

                    std::string p = path + "/" + ent->d_name;

                    //Aid es el nombre del fichero
                    //ext es la extensión
                    //p es la ruta completa del fichero
                    std::unordered_map<std::string, LoadFunc>::const_iterator it = loadFunctions.find(ext);
                    if(it != loadFunctions.end())
                    {
                      asset = it->second(p, Aid, ext, parser);
                      if(asset)
                      {
                        assets[Aid] = asset;
                      }
                    }
                }
        }

        delete parser;
        RF_Engine::Debug(("LoadAsset [Info]: " + id + " done"));
}

RF_AssetList::~RF_AssetList()
{
  assets.clear();
}

bool RF_AssetList::isLoaded(std::string id)
{
    return (assets.find(id) != assets.end());
}

int RF_AssetList::Size()
{
    return assets.size();
}

RF_Asset* loadAudio(std::string path, std::string Aid, std::string ext, Parser* parser)
{
  if(!parser->loaded)
  {
    RF_Engine::Debug("[Error] Falta package.json");
    return nullptr;
  }

  std::string opc;
  if(!Parser::json_string(parser->jsvalue[Aid], opc))
  {
    Parser::json_string(parser->jsvalue["AllAudio"], opc);
  }

  if(opc == "fx")
  {
      return new RF_FXClip(Aid, Mix_LoadWAV(path.c_str()));
  }
  else
  {
      return new RF_AudioClip(Aid, Mix_LoadMUS(path.c_str()));
  }
}

RF_Asset* loadGfx2D(std::string path, std::string Aid, std::string ext, Parser* parser)
{
  return new RF_Gfx2D(Aid, RF_Primitive::loadPNG_Surface(path.c_str()));
}

RF_Asset* loadFont(std::string path, std::string Aid, std::string ext, Parser* parser)
{
  int pt;
  if(!parser->loaded || !Parser::json_int(parser->jsvalue[Aid], pt))
  {
    pt = 12;
  }

  return new RF_Font(Aid, TTF_OpenFont(path.c_str(),pt), path);
}

std::unordered_map<std::string, LoadFunc> RF_AssetList::loadFunctions {
  {"png", &loadGfx2D},
  {"jpg", &loadGfx2D},
  {"wav", &loadAudio},
  {"mp3", &loadAudio},
  {"ogg", &loadAudio},
  {"ttf", &loadFont}
};

void RF_AssetList::addResource(std::string extension, LoadFunc function)
{
  RF_AssetList::loadFunctions[extension] = function;
}
