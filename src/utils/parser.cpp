#include "utils/parser.h"

#include "rf_engine.h"
#include <iostream>
#include <fstream>

bool Parser::json_string(Json::Value & o, std::string & res)
{
	if (o.isNull()) return false;
	if (!o.isString()) return false;
	res = o.asString();
	return true;
}

bool Parser::json_float(Json::Value & o, float &res)
{
	if (o.isNull()) return false;
	if (!o.isDouble()) return false;
	res = o.asFloat();
	return true;
}

bool Parser::json_int(Json::Value & o, int & res)
{
	if (o.isNull()) return false;
	if (!o.isIntegral()) return false;
	res = o.asInt();
	return true;
}

bool Parser::json_bool(Json::Value & o, bool & res)
{
	if (o.isNull()) return false;
	if (!o.isBool()) return false;
	res = o.asBool();
	return true;
}

bool Parser::json_vector2int(Json::Value & o, RF_Structs::Vector2<int> &res)
{
	if (o.isNull()) return false;
	if (!o.isArray()) return false;
	if (o.size() < 2)  return false;
	if (!o[0].isNumeric()) return false;
	if (!o[1].isNumeric()) return false;
	res = RF_Structs::Vector2<int>(o[0].asInt(), o[1].asInt());
	return true;
}

bool Parser::json_vector2float(Json::Value & o, RF_Structs::Vector2<float> &res)
{
	if (o.isNull()) return false;
	if (!o.isArray()) return false;
	if (o.size() < 2)  return false;
	if (!o[0].isNumeric()) return false;
	if (!o[1].isNumeric()) return false;
	res = RF_Structs::Vector2<float>(o[0].asFloat(), o[1].asFloat());
	return true;
}

bool Parser::json_SDLColor(Json::Value & o, SDL_Color &res)
{
  if (o.isNull()) return false;
  if (!o.isArray()) return false;
  if (o.size() < 3)  return false;
  if (!o[0].isIntegral()) return false;
  if (!o[1].isIntegral()) return false;
  if (!o[2].isIntegral()) return false;
  res = {(Uint8)o[0].asInt(), (Uint8)o[1].asInt(), (Uint8)o[2].asInt()};
  return true;
}

Parser::Parser(std::string file)
{
  std::ifstream is;
	is.open(file.c_str(), std::ifstream::in);

	if(!is)
  {
		RF_Engine::Debug("[E] Can't open " + file);
    return;
	}

	is >> jsvalue;
	loaded = true;
}

Parser::~Parser()
{

}
