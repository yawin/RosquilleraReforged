/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "rf_input.h"
#include "rf_engine.h"
using namespace RF_Structs;

#include <cmath>
using string = std::string;

bool RF_Input::key[_FOO_KEY] {};
SDL_Event RF_Input::Private::event;
RF_Mouse RF_Input::mouse;
SDL_Joystick* RF_Input::gGameController {nullptr};
const int RF_Input::Private::JOYSTICK_DEAD_ZONE {8000};
Vector2<int> RF_Input::jDir;
bool RF_Input::jkey[_FOO_JKEY] {};
bool RF_Input::jPlugged {false};

void RF_Input::Update()
{
  using namespace RF_Input::Private;

  SDL_PumpEvents();

  const Uint8 *keys=SDL_GetKeyboardState(NULL);

  key[_a] = keys[SDL_SCANCODE_A];
  key[_b] = keys[SDL_SCANCODE_B];
  key[_c] = keys[SDL_SCANCODE_C];
  key[_d] = keys[SDL_SCANCODE_D];
  key[_e] = keys[SDL_SCANCODE_E];
  key[_f] = keys[SDL_SCANCODE_F];
  key[_g] = keys[SDL_SCANCODE_G];
  key[_h] = keys[SDL_SCANCODE_H];
  key[_i] = keys[SDL_SCANCODE_I];
  key[_j] = keys[SDL_SCANCODE_J];
  key[_k] = keys[SDL_SCANCODE_K];
  key[_l] = keys[SDL_SCANCODE_L];
  key[_m] = keys[SDL_SCANCODE_M];
  key[_n] = keys[SDL_SCANCODE_N];
  //key[_ñ] = keys[SDL_SCANCODE_];
  key[_o] = keys[SDL_SCANCODE_O];
  key[_p] = keys[SDL_SCANCODE_P];
  key[_q] = keys[SDL_SCANCODE_Q];
  key[_r] = keys[SDL_SCANCODE_R];
  key[_s] = keys[SDL_SCANCODE_S];
  key[_t] = keys[SDL_SCANCODE_T];
  key[_u] = keys[SDL_SCANCODE_U];
  key[_v] = keys[SDL_SCANCODE_V];
  key[_w] = keys[SDL_SCANCODE_W];
  key[_x] = keys[SDL_SCANCODE_X];
  key[_y] = keys[SDL_SCANCODE_Y];
  key[_z] = keys[SDL_SCANCODE_Z];

  key[_0] = keys[SDL_SCANCODE_0];
  key[_1] = keys[SDL_SCANCODE_1];
  key[_2] = keys[SDL_SCANCODE_2];
  key[_3] = keys[SDL_SCANCODE_3];
  key[_4] = keys[SDL_SCANCODE_4];
  key[_5] = keys[SDL_SCANCODE_5];
  key[_6] = keys[SDL_SCANCODE_6];
  key[_7] = keys[SDL_SCANCODE_7];
  key[_8] = keys[SDL_SCANCODE_8];
  key[_9] = keys[SDL_SCANCODE_9];

  key[_return] = keys[SDL_SCANCODE_RETURN];
  key[_esc] = keys[SDL_SCANCODE_ESCAPE];
  key[_backspace] = keys[SDL_SCANCODE_BACKSPACE];
  key[_tab] = keys[SDL_SCANCODE_TAB];
  key[_space] = keys[SDL_SCANCODE_SPACE];
  key[_minus] = keys[SDL_SCANCODE_MINUS];
  key[_equals] = keys[SDL_SCANCODE_EQUALS];
  key[_leftbracket] = keys[SDL_SCANCODE_LEFTBRACKET];
  key[_rightbracket] = keys[SDL_SCANCODE_RIGHTBRACKET];
  key[_backslash] = keys[SDL_SCANCODE_BACKSLASH];
  key[_nonushash] = keys[SDL_SCANCODE_NONUSHASH];
  key[_semicolon] = keys[SDL_SCANCODE_SEMICOLON];
  key[_apostrophe] = keys[SDL_SCANCODE_APOSTROPHE];
  key[_grave] = keys[SDL_SCANCODE_GRAVE];
  key[_comma] = keys[SDL_SCANCODE_COMMA];
  key[_period] = keys[SDL_SCANCODE_PERIOD];
  key[_slash] = keys[SDL_SCANCODE_SLASH];

  key[_left_shift] = keys[SDL_SCANCODE_LSHIFT];
  key[_right_shift] = keys[SDL_SCANCODE_RSHIFT];

  key[_left] = keys[SDL_SCANCODE_LEFT];
  key[_right] = keys[SDL_SCANCODE_RIGHT];
  key[_up] = keys[SDL_SCANCODE_UP];
  key[_down] = keys[SDL_SCANCODE_DOWN];

  while (SDL_PollEvent(&event))
  {
    switch (event.type)
    {
      case SDL_QUIT: //Si pulsamos el botón de cerrar ventana
      key[_close_window]=true;
      break;

      case SDL_MOUSEMOTION:
      SDL_GetMouseState(&mouse.position.x,&mouse.position.y);
      break;

      case SDL_MOUSEBUTTONDOWN:
      {
        int button = 0;
        switch(event.button.button)
        {
          case SDL_BUTTON_LEFT:
          button = 0;
          break;

          case SDL_BUTTON_RIGHT:
          button = 1;
          break;

          default:
          RF_Engine::Debug("Este botón del ratón aún no está soportado");
          break;
        }
        mouse.Click[button] = true;
      }
      break;

      case SDL_MOUSEBUTTONUP:
      {
        int button = 0;
        switch (event.button.button)
        {
          case SDL_BUTTON_LEFT:
          button = _leftbutton;
          break;

          case SDL_BUTTON_RIGHT:
          button = _rightbutton;
          break;

          default:
          RF_Engine::Debug("Este botón del ratón aún no está soportado");
          break;
        }
        mouse.Click[button] = false;
      }
      break;

      case SDL_JOYBUTTONUP:
      case SDL_JOYBUTTONDOWN:
      /*if(event.jaxis.which == 0)
      {*/
        if((int)event.jbutton.button < _FOO_JKEY-4)
          jkey[(int)event.jbutton.button] = (event.jbutton.state == SDL_PRESSED);
        //}
        break;

      case SDL_JOYDEVICEADDED:
      case SDL_JOYDEVICEREMOVED:
        jPlugged = (SDL_JOYDEVICEADDED == event.type);
        break;
      case SDL_JOYAXISMOTION:
        /*if(event.jaxis.which == 0)
        {*/
          if(event.jaxis.axis == 0)
          {
            //Left of dead zone
            if(event.jaxis.value < -JOYSTICK_DEAD_ZONE)
            {
              jDir.x = -1;
            }

            //Right of dead zone
            else if(event.jaxis.value > JOYSTICK_DEAD_ZONE)
            {
              jDir.x = 1;
            }
            else
            {
              jDir.x = 0;
            }
          }
          else if(event.jaxis.axis == 1)
          {
            //Below of dead zone
            if(event.jaxis.value < -JOYSTICK_DEAD_ZONE)
            {
              jDir.y = -1;
            }
            //Above of dead zone
            else if(event.jaxis.value > JOYSTICK_DEAD_ZONE)
            {
              jDir.y = 1;
            }
            else
            {
              jDir.y = 0;
            }
          }
          //}

          jkey[_jpad_dir_left] = (jDir.x < 0);
          jkey[_jpad_dir_right] = (jDir.x > 0);
          jkey[_jpad_dir_up] = (jDir.y < 0);
          jkey[_jpad_dir_down] = (jDir.y > 0);
          break;
        }
      }

      /*double joystickAngle = atan2( (double)jDir.y, (double)jDir.x) * ( 180.0 / M_PI );
      if(jDir.x == 0 && jDir.y == 0)
      {
        joystickAngle = 0;
      }*/
};
