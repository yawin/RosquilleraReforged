/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "rf_process.h"

#include <cmath>
using string = std::string;

const SDL_RendererFlip FLIP_NONE {SDL_FLIP_NONE};
const SDL_RendererFlip FLIP_H {SDL_FLIP_HORIZONTAL};
const SDL_RendererFlip FLIP_V {SDL_FLIP_VERTICAL};

RF_Process::RF_Process(string _type, RF_Window *_window) : type(_type), window(_window){}
RF_Process::~RF_Process(){}

void RF_Process::Start(){ return; }
void RF_Process::Update(){ return; }
void RF_Process::FixedUpdate(){ return; }
void RF_Process::Draw(){ return; }
void RF_Process::LateDraw(){ return; }
void RF_Process::AfterDraw(){ return; }
void RF_Process::Dispose(){ return; }

bool RF_Process::operator<(const RF_Process* p) const
{
  return (zLayer < p->zLayer);
}

SDL_Rect RF_Process::getDimensions()
{
  if(graph == nullptr){return {(int)transform.position.x, (int)transform.position.y, 0, 0};}

  private_width  = (graph->w>>1) * transform.scale.x;
  private_height = (graph->h>>1) * transform.scale.y;
  private_rotation = transform.rotation * TO_RADIAN;

  private_xa = private_xb = private_vertices[0][0]*private_width*cos(private_rotation) - private_vertices[0][1]*private_height*sin(private_rotation);
  private_ya = private_yb = private_vertices[0][0]*private_width*sin(private_rotation) + private_vertices[0][1]*private_height*cos(private_rotation);

  for(int i = 1; i < 4; ++i)
  {
    private_x = private_vertices[i][0]*private_width*cos(private_rotation) - private_vertices[i][1]*private_height*sin(private_rotation);
    private_y = private_vertices[i][0]*private_width*sin(private_rotation) + private_vertices[i][1]*private_height*cos(private_rotation);

    private_xa = (private_xa < private_x) ? private_xa : private_x;
    private_ya = (private_ya < private_y) ? private_ya : private_y;
    private_xb = (private_xb > private_x) ? private_xb : private_x;
    private_yb = (private_yb > private_y) ? private_yb : private_y;
  }

  private_width = private_xb - private_xa;
  private_height = private_yb - private_ya;

  return {(int)(transform.position.x - private_width*0.5),(int)(transform.position.y - private_height*0.5), (int)private_width, (int)private_height};
}

RF_Structs::Vector2<int> RF_Process::checkPoint(RF_Structs::Vector2<int> p)
{
  return checkPoint(p.x, p.y);
}
RF_Structs::Vector2<int> RF_Process::checkPoint(int p_x, int p_y)
{
  if(graph == nullptr){ return {-1,-1}; }

  private_rotation = transform.rotation * TO_RADIAN;

  private_xa = (float)p_x - transform.position.x;
  private_ya = (float)p_y - transform.position.y;

  private_x = private_xa*cos(private_rotation) + private_ya*sin(private_rotation);
  private_y = -private_xa*sin(private_rotation) + private_ya*cos(private_rotation);

  private_x = private_x / transform.scale.x;
  private_y = private_y / transform.scale.y;

  private_x +=  graph->w>>1;
  private_y += graph->h>>1;

  if(private_x < 0 || private_x > graph->w){ private_x = -1; }
  if(private_y < 0 || private_y > graph->h){ private_y = -1; }

  return {(int)private_x, (int)private_y};
}
