/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "rf_collision.h"
#include "rf_primitive.h"

SDL_Rect RF_Collision::getIntersection(SDL_Rect boundA, SDL_Rect boundB)
{
  int x1 = Maximum(boundA.x, boundB.x);
  int y1 = Maximum(boundA.y, boundB.y);
  int x2 = Minimum(boundA.x + boundA.w, boundB.x + boundB.w);
  int y2 = Minimum(boundA.y + boundA.h, boundB.y + boundB.h);

  int width = x2 - x1;
  int height = y2 - y1;

  SDL_Rect intersect = {0,0,0,0};
  if(width > 0 && height > 0)
  {
    intersect.x = x1;
    intersect.y = y1;
    intersect.w = width;
    intersect.h = height;
  }

  return intersect;
}

bool RF_Collision::checkCollision(RF_Process* entityA, RF_Process* entityB, int collisionType)
{
    SDL_Rect collisionRect = getIntersection(entityA->getDimensions(), entityB->getDimensions());

    //Si la colisión es de tipo AABB
      if(collisionType == RF_CT_AABB)
        return !(collisionRect.w == 0 && collisionRect.h == 0);

    //Si es Pixel Perfect continuamos

    if(collisionRect.w == 0 && collisionRect.h == 0)
        return false;

    Uint8 foo, alphaA, alphaB;
    RF_Structs::Vector2<int> pixels[2];

    for(int j = 0; j < collisionRect.h; ++j)
    {
      for(int i = 0; i < collisionRect.w; ++i)
      {
        pixels[0] = entityA->checkPoint(collisionRect.x+i, collisionRect.y+j);
        pixels[1] = entityB->checkPoint(collisionRect.x+i, collisionRect.y+j);

        if(pixels[0].x >= 0 && pixels[0].y >= 0 && pixels[1].x >= 0 && pixels[1].y >= 0)
        {
          SDL_GetRGBA(
            RF_Primitive::getPixel(entityA->graph, pixels[0].x, pixels[0].y),
            entityA->graph->format,
            &foo, &foo, &foo, &alphaA);

          SDL_GetRGBA(
            RF_Primitive::getPixel(entityB->graph, pixels[1].x, pixels[1].y),
            entityB->graph->format,
            &foo, &foo, &foo, &alphaB);

          if(alphaA > 200 && alphaB > 200)
          {
            return true;
          }
        }
      }
    }

    return false;
}

RF_Process* RF_Collision::checkCollisionByType(RF_Process* entityA, string type, int collisionType)
{
  for(RF_Process* entityB : RF_TaskManager::instance->getTaskByType(type))
  {
    if(checkCollision(entityA, entityB, collisionType))
    {
      return entityB;
    }
  }

  return nullptr;
}
