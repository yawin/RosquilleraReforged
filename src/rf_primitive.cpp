/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "rf_primitive.h"

void RF_Primitive::drawLine(SDL_Surface* surf, RF_Structs::Vector2<int> p0, RF_Structs::Vector2<int> p1, Uint32 color)
{
  int dX = p1.x - p0.x;
  int dY = p1.y - p0.y;

  float pasos = abs(dX);
  if(abs(dY) >= abs(dX))
  {
    pasos = abs(dY);
  }

  RF_Structs::Vector2<float> inc = RF_Structs::Vector2<float>(dX/pasos, dY/pasos);
  RF_Structs::Vector2<float> pF = RF_Structs::Vector2<float>((float)p0.x,(float)p0.y);
  for(int i = 0; i < pasos; ++i)
  {
    RF_Primitive::putPixel(surf,(int)pF.x,(int)pF.y,color);
    pF.x += inc.x;
    pF.y += inc.y;
  }
}

void RF_Primitive::drawFCircle(SDL_Surface* surf, RF_Structs::Vector2<int> p, int r, Uint32 color)
{
  for(float i = 0; i <= r; i+=0.1)
  {
    float j2 = r*r-i*i;
    float j = sqrt(j2);

    drawLine(surf,RF_Structs::Vector2<int>(p.x-round(i),p.y-round(j)), RF_Structs::Vector2<int>(p.x+round(i),p.y-round(j)),color);
    drawLine(surf,RF_Structs::Vector2<int>(p.x-round(i),p.y+round(j)), RF_Structs::Vector2<int>(p.x+round(i),p.y+round(j)),color);
  }
}

void RF_Primitive::drawCircle(SDL_Surface* surf, RF_Structs::Vector2<int> p, int r, Uint32 color)
{
  for(float i = 0; i <= r; i+=0.1)
  {
    float j2 = r*r-i*i;
    float j = sqrt(j2);

    putPixel(surf,p.x+round(i),p.y+round(j),color);
    putPixel(surf,p.x-round(i),p.y+round(j),color);

    putPixel(surf,p.x+round(i),p.y-round(j),color);
    putPixel(surf,p.x-round(i),p.y-round(j),color);
  }
}

Uint32 RF_Primitive::getPixel(SDL_Surface* surface, int x, int y)
{
  //Convert the pixels to 32 bit
    //Uint32 *pixels = (Uint32 *)surface->pixels;

  //Get the requested pixel
    //return pixels[(y * surface->w) + x];

    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to retrieve */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp)
    {
      case 1:
        return *p;
        break;

      case 2:
        return *(Uint16 *)p;
        break;

      case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
        {
          return p[0] | p[1] << 8 | p[2] << 16;
        }
        else
        {
          return p[0] << 16 | p[1] << 8 | p[2];
        }
        break;

      case 4:
        return *(Uint32 *)p;//p[0] << 16 | p[1] << 8 | p[2] | p[3] << 32;//return *(Uint32 *)p;
        break;

      default:
        return 0;
    }
}

void RF_Primitive::putPixel(SDL_Surface* surface, int x, int y, Uint32 pixel)
{
  if(0 > x || 0 > y || surface->w <= x || surface->h <= y){return;}

  //Convert the pixels to 32 bit
    //Uint32 *pixels = (Uint32 *)surface->pixels;

  //Set the pixel
    //pixels[(y * surface->w) + x] = pixel;

  int bpp = surface->format->BytesPerPixel;
  Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

  switch(bpp)
  {
    case 1:
    *p = pixel;
    break;

    case 2:
    *(Uint16 *)p = pixel;
    break;

    case 3:
    if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
    {
      p[0] = (pixel >> 16) & 0xff;
      p[1] = (pixel >> 8) & 0xff;
      p[2] = pixel & 0xff;
    }
    else
    {
      p[0] = pixel & 0xff;
      p[1] = (pixel >> 8) & 0xff;
      p[2] = (pixel >> 16) & 0xff;
    }
    break;

    case 4:
    *(Uint32 *)p = pixel;
    break;
  }


}

SDL_Surface* RF_Primitive::loadPNG_Surface(string file)
{
  return IMG_Load(file.c_str());
}

SDL_Texture* RF_Primitive::loadPNG(string file, RF_Window* ventana)
{
  return IMG_LoadTexture(ventana->renderer,file.c_str());
}

void RF_Primitive::resizeSurface(SDL_Surface* original, SDL_Surface* final)
{
  RF_Structs::Vector2<float> *scale = new RF_Structs::Vector2<float>(final->w/original->w, final->h/original->h);
  if(scale->x == 0 || scale->y == 0)
  {
    SDL_FreeSurface(final);
    final = SDL_CreateRGBSurface(0, original->w, original->h, 32, 0, 0, 0, 0);
    SDL_BlitSurface(original, NULL, final, NULL);

    return;
  }

  for(int j = 0; j < final->h; ++j)
  {
    for(int i = 0; i < final->w; ++i)
    {
      RF_Primitive::putPixel(final, i, j, RF_Primitive::getPixel(original, i/scale->x, j/scale->y));
    }
  }
}

void RF_Primitive::clearSurface(SDL_Surface* srf, Uint32 color)
{
  for(int j = 0; j < srf->h; j+=3)
  {
    for(int i = 0; i < srf->w; i+=3)
    {
      putPixel(srf, i,j,color);    putPixel(srf, i,j+1,color);    putPixel(srf, i,j+2,color);
      putPixel(srf, i+1,j,color);  putPixel(srf, i+1,j+1,color);  putPixel(srf, i+1,j+2,color);
      putPixel(srf, i+2,j,color);  putPixel(srf, i+2,j+1,color);  putPixel(srf, i+2,j+2,color);
    }
  }
}
