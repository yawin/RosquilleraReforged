/*
LA ROSQUILLERA FRAMEWORK - REFORGED
Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#include "rf_window.h"
#include "rf_primitive.h"
#include "rf_textmanager.h"
#include <iostream>
using string = std::string;

RF_Window::RF_Window(string i_title, int i_windowMode, int i_posX, int i_posY, int i_width, int i_height, int i_rendererMode)
{
  _title=i_title;
  _windowMode=i_windowMode | SDL_WINDOW_OPENGL | SDL_WINDOW_MOUSE_FOCUS;
  transform.position.x=i_posX;
  transform.position.y=i_posY;
  transform.scale.x=i_width;
  transform.scale.y=i_height;
  _rendererMode=i_rendererMode;
  _index=0;

  window = SDL_CreateWindow(_title.c_str(), transform.position.x, transform.position.y, transform.scale.x, transform.scale.y, _windowMode);
  renderer = SDL_CreateRenderer(window,_index,_rendererMode);

  SDL_GetWindowPosition(window, &realTransform.position.x, &realTransform.position.y);
  SDL_GetWindowSize(window, &realTransform.scale.x, &realTransform.scale.y);

  setResolution(transform.scale.x, transform.scale.y);
}

RF_Window::~RF_Window()
{
  RF_TextManager::CleanWindow(this);
  SDL_DestroyWindow(window);
  SDL_DestroyRenderer(renderer);
}

void RF_Window::Rend(SDL_Surface *srf, RF_Structs::Transform2D<float, float, float> *transform, SDL_RendererFlip flipType)
{
  _Rend(srf, transform->position, transform->scale, transform->rotation, flipType);
}
void RF_Window::Rend(SDL_Surface *srf, RF_Structs::Transform2D<int, float, float> *transform, SDL_RendererFlip flipType)
{
  _Rend(srf, RF_Structs::Vector2<float>((float)transform->position.x, (float)transform->position.y), transform->scale, transform->rotation, flipType);
}

void RF_Window::_Rend(SDL_Surface *srf, RF_Structs::Vector2<float> position, RF_Structs::Vector2<float> scale, float rotation, SDL_RendererFlip flipType)
{
  if(hidden) return;

  if(!prepared)
  {
    SDL_RenderClear(renderer);
    prepared = true;
  }

  texture = SDL_CreateTextureFromSurface(renderer, srf);

  //SDL_QueryTexture(texture,NULL,NULL,&r.w,&r.h);
  r_w = (float)srf->w * scale.x * rendModifier.scale.x;
  r_h = (float)srf->h * scale.y * rendModifier.scale.y;
  r_x = ((!horizontal)? (float)aspectRatioSafer[0].w : 0) + (position.x * rendModifier.position.x) - r_w*0.5;
  r_y = ((horizontal)? (float)aspectRatioSafer[0].h : 0) + (position.y * rendModifier.position.y) - r_h*0.5;

  rect.x = r_x; rect.y = r_y;
  rect.w = r_w; rect.h = r_h;

  //SDL_RenderCopy(renderer,texture,NULL,&r);
  SDL_RenderCopyEx(renderer, texture, NULL, &rect, (float)rotation, NULL, flipType);
  SDL_DestroyTexture(texture);
}
void RF_Window::doRend()
{
  if(hidden) return;

  if(!prepared)
  {
    SDL_RenderClear(renderer);
  }

  SDL_GetRenderDrawColor(renderer, &r, &g, &b, &a);
  SDL_SetRenderDrawColor(renderer,0,0,0,255);

  SDL_RenderFillRect(renderer, &aspectRatioSafer[0]);
  SDL_RenderFillRect(renderer, &aspectRatioSafer[1]);

  SDL_SetRenderDrawColor(renderer,r,g,b,a);

  SDL_RenderPresent(renderer);
  prepared = false;
}

void RF_Window::setBackColor(Uint8 r, Uint8 g, Uint8 b)
{
  // Select the color for drawing. It is set to red here.
  SDL_SetRenderDrawColor(renderer, r, g, b, 255);

  // Clear the entire screen to our selected color.
  SDL_RenderClear(renderer);
}

void RF_Window::move(RF_Structs::Vector2<int> pos)
{
  move(pos.x, pos.y);
}

void RF_Window::move(int x, int y)
{
  transform.position.x = x;
  transform.position.y = y;

  SDL_SetWindowPosition(window, transform.position.x, transform.position.y);
}

bool RF_Window::hasFocus()
{
  Uint32 flags = SDL_GetWindowFlags(window);
  return (SDL_WINDOW_INPUT_FOCUS == (flags & SDL_WINDOW_INPUT_FOCUS));
}

void RF_Window::setVisibility(bool visibility)
{
  hidden = !visibility;

  if(hidden)
  SDL_HideWindow(window);
  else
  SDL_ShowWindow(window);
}

bool RF_Window::getVisibility()
{
  return !hidden;
}

void RF_Window::resize(RF_Structs::Vector2<int> scal)
{
  resize(scal.x, scal.y);
}

void RF_Window::resize(int x, int y)
{
  transform.scale.x = x;
  transform.scale.y = y;

  SDL_SetWindowSize(window, transform.scale.x, transform.scale.y);
}

void RF_Window::setResolution(RF_Structs::Vector2<int> size, bool scaled)
{
  setResolution(size.x, size.y, scaled);
}

void RF_Window::setResolution(int width, int height, bool scaled)
{
  if(!scaled) resize(width, height);

  transform.scale.x = width;
  transform.scale.y = height;

  //Aspect ratio
  rendModifier.scale.x = (float)realTransform.scale.x / (float)transform.scale.x;
  rendModifier.scale.y = (float)realTransform.scale.y / (float)transform.scale.y;

  horizontal = (rendModifier.scale.x < rendModifier.scale.y);

  if(horizontal) rendModifier.scale.y = rendModifier.scale.x;
  else rendModifier.scale.x = rendModifier.scale.y;

  aspectRatioSafer[0].x = aspectRatioSafer[1].x = 0;
  aspectRatioSafer[0].y = aspectRatioSafer[1].y = 0;
  aspectRatioSafer[0].w = aspectRatioSafer[1].w = realTransform.scale.x;
  aspectRatioSafer[0].h = aspectRatioSafer[1].h = realTransform.scale.y;

  if(horizontal)
  {
    aspectRatioSafer[0].h = aspectRatioSafer[1].h = (realTransform.scale.y - transform.scale.y*rendModifier.scale.y)*0.5;
    aspectRatioSafer[1].y = realTransform.scale.y - aspectRatioSafer[1].h;
  }
  else
  {
    aspectRatioSafer[0].w = aspectRatioSafer[1].w = (realTransform.scale.x - transform.scale.x*rendModifier.scale.x)*0.5;
    aspectRatioSafer[1].x = realTransform.scale.x - aspectRatioSafer[1].w;
  }

  rendModifier.position.x = ((float)realTransform.scale.x - ((!horizontal)? (float)aspectRatioSafer[0].w*2.0 : 0)) / (float)transform.scale.x;
  rendModifier.position.y = ((float)realTransform.scale.y - ((horizontal)? (float)aspectRatioSafer[0].h*2.0 : 0)) / (float)transform.scale.y;
}
