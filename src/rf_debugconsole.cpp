/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "rf_debugconsole.h"
#include "rf_engine.h"
#include "rf_textmanager.h"
#include <iostream>
#include <fstream>

RF_DebugCommand::RF_DebugCommand(const char* d, int n, RF_DebugCommandCallback cback): description (std::string(d)), nargs (n), callback (cback){}

void RF_DebugConsoleHelpCommand(int argc, const char* argv[])
{
  RF_DebugConsoleListener::writeLine("Lista de comandos");
  for(auto it = RF_DebugConsoleListener::instance->commands.begin(); it != RF_DebugConsoleListener::instance->commands.end(); ++it)
  {
    if(it->second != nullptr)
      RF_DebugConsoleListener::writeLine("  " + it->first + " - " + it->second->description);
  }
}

RF_DebugConsoleListener* RF_DebugConsoleListener::instance {nullptr};
std::map<std::string, RF_DebugCommand*> RF_DebugConsoleListener::commands;
std::vector<std::string> RF_DebugConsoleListener::log;

RF_DebugConsoleListener::RF_DebugConsoleListener():RF_Process("RF_DebugConsoleListener"){}
RF_DebugConsoleListener::~RF_DebugConsoleListener()
{
  if(instance->id == id)
  {
    instance = nullptr;

    SDLNet_UDP_Close(rec_sock);
    SDLNet_Quit();
  }
}

void RF_DebugConsoleListener::Start()
{
  if(instance != nullptr)
  {
    signal = RF_Structs::S_KILL;
    return;
  }

  instance = this;

  addCommand("help", new RF_DebugCommand("muestra esta ayuda", 0, &RF_DebugConsoleHelpCommand));

  prepareSocket();
}

void RF_DebugConsoleListener::Update()
{
  if(!computing && log.size() > 0)
  {
    for(std::string s : log)
    {
      _writeLine(s);
    }
    log.clear();
  }
}

void RF_DebugConsoleListener::checkCommand(std::string command)
{
  std::vector<std::string> c = RF_Structs::splitLine(command, " ");

  if(c[0] == "")
  {
    writeLine("Debes introducir un comando");
    return;
  }

  if(commands[c[0]] == nullptr)
  {
    writeLine("No se ha encontrado el comando \"" + c[0] + "\"");
    return;
  }

  int argc = c.size()-1;
  const char *argv[argc];
  for(int i = 0; i < argc; ++i)
  {
    argv[i] = c[i+1].c_str();
  }

  RF_DebugCommand* com = commands[c[0]];
  if(argc < com->nargs){writeLine("Faltan argumentos");}
  else if(argc > com->nargs){writeLine("Sobran argumentos");}
  else{com->callback(argc, argv);}

  return;
}

void RF_DebugConsoleListener::Log(std::string text)
{
  std::cout << text << "\n";

  std::ofstream pf;
    pf.open("./rosqui.log", std::ofstream::out | std::ofstream::app);
    pf << "[" << SDL_GetTicks() << "]: " << text << "\n";
    pf.close();

  RF_DebugConsoleListener::log.push_back("[LOG]" + text);
}

void RF_DebugConsoleListener::addCommand(std::string command, RF_DebugCommand* dc)
{
  commands[command] = dc;
}

void RF_DebugConsoleListener::writeLine(std::string text)
{
  RF_DebugConsoleListener::instance->_writeLine("[COM]" + text);
}

void RF_DebugConsoleListener::_writeLine(std::string text)
{
  UDPpacket paquete;
  paquete.address = send_addr;
  paquete.data = (Uint8*)text.c_str();
  paquete.len = text.size();
  SDLNet_UDP_Send(rec_sock, -1, &paquete);
}

void RF_DebugConsoleListener::prepareSocket()
{
    SDLNet_Init();
    RF_Engine::Debug("SDLNet_Init()");

  //Bindeamos escucha
    rec_sock=SDLNet_UDP_Open(rec_port);
    if(!rec_sock)
    {
      RF_Engine::Debug("[Error] No se ha levantado RecSocket");
    }
    else
    {
      RF_Engine::Debug("RecSocket levantado en el puerto " + std::to_string(rec_port));
    }

  //Generamos IPaddress de envío
    SDLNet_ResolveHost(&send_addr, ip.c_str(), send_port);

  //Reservamos paquete
    rec_packet = SDLNet_AllocPacket(DEBUG_BUFLEN);

  instance = this;
  //pthread_create(&listener, NULL, socketListen, NULL);
  listener = SDL_CreateThread( socketListen, "socketListen", (void*)NULL );
}

int socketListen(void *v)
{
  RF_DebugConsoleListener::instance->Listen();
  return 0;
}
void RF_DebugConsoleListener::Listen()
{
  do
  {
    if(SDLNet_UDP_Recv(rec_sock, rec_packet))
    {
      computing = true;
      command = "";
      for(int i = 0; i < rec_packet->len; ++i)
      {
        command += rec_packet->data[i];
      }

      RF_DebugConsoleListener::instance->checkCommand(command);
      writeLine("DONE");
      computing = false;
    }
  } while(instance->signal == RF_Structs::S_AWAKE && RF_Engine::Status());
  //pthread_exit(NULL);
}

std::string RF_DebugConsoleListener::ip {"255.255.255.255"};
int RF_DebugConsoleListener::rec_port {50002};
int RF_DebugConsoleListener::send_port {50003};
