/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef RF_WINDOW_H
#define RF_WINDOW_H

class RF_Process;
#include "rf_structs.h"
#include <SDL2/SDL.h>
#include <string>
#include <vector>
using string = std::string;

class RF_Window
{
    public:
        RF_Window(string i_title, int i_windowMode, int i_posX, int i_posY, int i_width, int i_height, int i_rendererMode);
        virtual ~RF_Window();

        void Rend(SDL_Surface *srf, RF_Structs::Transform2D<int, float, float> *position, SDL_RendererFlip flipType = SDL_FLIP_NONE);
        void Rend(SDL_Surface *srf, RF_Structs::Transform2D<float, float, float> *position, SDL_RendererFlip flipType = SDL_FLIP_NONE);
        void _Rend(SDL_Surface *srf, RF_Structs::Vector2<float> position, RF_Structs::Vector2<float> scale, float rotation, SDL_RendererFlip flipType = SDL_FLIP_NONE);

        void doRend();

        const int& width() const {return transform.scale.x;}
        const int& height() const {return transform.scale.y;}
        const int& realWidth() const {return realTransform.scale.x;}
        const int& realHeight() const {return realTransform.scale.y;}

        const int& x() const {return transform.position.x;}
        const int& y() const {return transform.position.y;}
        const int& realX() const {return realTransform.position.x;}
        const int& realY() const {return realTransform.position.y;}

        SDL_Window* Window() {return window;}

        const string& title() const {return _title;}
        void title(string newTitle)
        {
            _title = newTitle;
            SDL_SetWindowTitle(window, _title.c_str());
        }

        void setBackColor(Uint8 r, Uint8 g, Uint8 b);

        void move(RF_Structs::Vector2<int> pos);
        void move(int x, int y);
        void resize(RF_Structs::Vector2<int> scal);
        void resize(int x, int y);

        void setResolution(int width, int height, bool scaled = true);
        void setResolution(RF_Structs::Vector2<int> size, bool scaled = true);

        bool hasFocus();
        void setVisibility(bool visibility);
        bool getVisibility();

        SDL_Renderer*   renderer;

    private:
        string          _title;
        int             _index;
        int             _windowMode;
        int             _rendererMode;
        RF_Structs::Transform2D<int, int, int> transform;
        RF_Structs::Transform2D<int, int, int> realTransform;

        SDL_Window*     window;

        bool prepared = false;
        bool hidden = false;

        RF_Structs::Transform2D<float, float, float> rendModifier;
        SDL_Rect aspectRatioSafer[2];
        Uint8 r,g,b,a;
        bool horizontal = false;
        SDL_DisplayMode displayMode;

        SDL_Rect rect;
        SDL_Texture *texture;
        float r_x, r_y, r_w, r_h;
};

#endif // RF_WINDOW_H
