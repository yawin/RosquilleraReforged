/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/
#ifndef RF_SOUNDMANAGER_H
#define RF_SOUNDMANAGER_H

#include "rf_process.h"
#include <string>
#include <SDL2/SDL_mixer.h>
using string = std::string;

namespace RF_SoundManager
{
  namespace Private
  {
    class RF_SoundTransition : public RF_Process
    {
      public:
        RF_SoundTransition();

        virtual void Start();
        virtual void Update();

        bool changed {false};
    };

    extern Mix_Music* music;
    extern Mix_Music* cMusic;
    extern int volume;
  };

  //friend void RF_SoundTransition::Update();

  void playFX(string package, string file, int channel = -1);
  void playFX(Mix_Chunk* clip, int channel = -1);

  void playSong(string package, string file, int loop = -1);
  void playSong(Mix_Music* clip, int loop = -1);

  void changeMusic(string package, string file);
  void changeMusic(Mix_Music* clip);

};
#endif // RF_SOUNDMANAGER_H
