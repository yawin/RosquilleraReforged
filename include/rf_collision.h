/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef RF_COLLISION_H
#define RF_COLLISION_H

#include "rf_process.h"
#include "rf_taskmanager.h"

#include <string>
using string = std::string;

enum RF_Collision_Type
{
  RF_CT_AABB,
  RF_CT_PP
};

namespace RF_Collision
{
  #define Maximum(a, b) ((a > b) ? a : b)
  #define Minimum(a, b) ((a < b) ? a : b)

  SDL_Rect getIntersection(SDL_Rect boundA, SDL_Rect boundB);
  bool checkCollision(RF_Process* entityA, RF_Process* entityB, int collisionType = RF_CT_AABB);
  RF_Process* checkCollisionByType(RF_Process* entityA, string type, int collisionType = RF_CT_AABB);
};

#endif //RF_COLLISION_H
