/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.


    El objetivo del TaskManager es comunicarse con los procesos y transmitirles órdenes.
    Cuando el motor de la orden de actualizarse, transmitirá esa orden al TaskManager
*/

#ifndef RF_TASKMANAGER_H
#define RF_TASKMANAGER_H

#include "rf_process.h"
#include "rf_map.h"

#include <string>
#include <iostream>
#include <set>
#include <map>
using string = std::string;

class RF_TaskManager : public RF_Map
{
	public:
		static RF_TaskManager *instance;

		RF_TaskManager();
		virtual ~RF_TaskManager();

		template<typename T>
		string newTask(string father = "", RF_Window *_window = nullptr)
		{
			static_assert(std::is_base_of<RF_Process, T>::value, "[RF_TaskManager::newTask] T must derive from RF_Process");

			RF_Process *task = new T();
			int _id = 0;
			std::string id = father + ":" + task->type + std::to_string(_id);

			while(isInList(id))
			{
				++_id;
				id = father + ":" + task->type + std::to_string(_id);
			}

			Add(id, task);
			task->id = id;
			task->father = father;
			task->window = _window;
			task->zLayer = (isInList(father)) ? getTask(father)->zLayer+1 : 0;
			task->Start();
			return id;
		}

		bool existsTask(std::string id);
		RF_Process* getTask(std::string id);
		std::vector<RF_Process*> getTaskByType(std::string type);
		std::vector<RF_Process*> getTaskByFather(std::string father);
		std::vector<RF_Process*> getTaskList();
		std::vector<std::string> getTaskIdList();

		void sendSignal(std::string taskID, int signal);
		void sendSignal(RF_Process* task, int signal);

		void Update();
		void FixedUpdate();
		void Draw();

		void Clear();

    private:
        int cont, i;
				std::vector<RF_Process*> toRend;

				void manageSignals();

				void deleteTask(std::string id);
				void deleteTask(RF_Process* task);
};

#endif // RF_TASKMANAGER_H
