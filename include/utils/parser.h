#ifndef RF_PARSER_H
#define RF_PARSER_H

#include "./json.h"

#include "../rf_engine.h"
#include <string>

class Parser
{
  public:
    Parser(std::string file);
    virtual ~Parser();

    static bool json_string(Json::Value & o, std::string & res);
    static bool json_float(Json::Value & o, float &res);
    static bool json_int(Json::Value & o, int & res);
    static bool json_bool(Json::Value & o, bool & res);
    static bool json_vector2int(Json::Value & o, RF_Structs::Vector2<int> &res);
    static bool json_vector2float(Json::Value & o, RF_Structs::Vector2<float> &res);
    static bool json_SDLColor(Json::Value & o, SDL_Color &res);

  	Json::Value jsvalue;
    bool loaded = false;
};

#endif //RF_PARSER_H
