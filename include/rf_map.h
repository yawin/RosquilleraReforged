/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef RF_MAP_H
#define RF_MAP_H

#include "rf_process.h"
#include <iostream>
#include <vector>
#include <unordered_map>
using string = std::string;

struct Node
{
    string key;
    RF_Process* data;

    Node *son;
    Node *firstChild;

    void addChild(string key, RF_Process *dat)
    {
        Node *n = new Node();
        n->key = key;
        n->data = dat;

        n->son = firstChild;
        firstChild = n;
    }
};

class RF_Map
{
    protected:
        Node *root { new Node() };
        std::unordered_map<std::string, RF_Process*> taskMap;
        std::unordered_multimap<std::string, RF_Process*> typeMap;

        void Add(std::string key, RF_Process *dat);
        bool isInList(std::string key);
        RF_Process* Get(std::string key);
        void Remove(std::string key);
        void RemoveAll(std::string key);
        void _Remove(Node *aux, Node *father = nullptr);
        void RemoveAll();
        void split(const std::string& s, char delim,std::vector<std::string>& v);
};

#endif // RF_MAP_H
