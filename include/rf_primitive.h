/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef RF_PRIMITIVE_H
#define RF_PRIMITIVE_H

#include "rf_engine.h"
#include "rf_structs.h"
#include "rf_window.h"
#include <SDL2/SDL.h>
#include <math.h>
#include <string>
using string = std::string;

namespace RF_Primitive
{
  void drawLine(SDL_Surface* surf, RF_Structs::Vector2<int> p0, RF_Structs::Vector2<int> p1, Uint32 color);
  void drawFCircle(SDL_Surface* surf, RF_Structs::Vector2<int> p, int r, Uint32 color);
  void drawCircle(SDL_Surface* surf, RF_Structs::Vector2<int> p, int r, Uint32 color);

  Uint32 getPixel(SDL_Surface* surface, int x, int y);
  void putPixel(SDL_Surface* surface, int x, int y, Uint32 pixel);

  SDL_Surface* loadPNG_Surface(string file);
  SDL_Texture* loadPNG(string file, RF_Window* ventana);

  void resizeSurface(SDL_Surface* original, SDL_Surface* final);
  void clearSurface(SDL_Surface* srf, Uint32 color);
};

#endif // RF_PRIMITIVE_H
