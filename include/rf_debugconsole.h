/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef RF_DEBUGCONSOLE_H
#define RF_DEBUGCONSOLE_H

#include "rf_process.h"
#include <SDL2/SDL_net.h>
#include <SDL2/SDL_thread.h>

#include <map>
using string = std::string;

typedef void (*RF_DebugCommandCallback)(int, const char**);
struct RF_DebugCommand
{
  std::string description;
  int nargs;
  RF_DebugCommandCallback callback;

  RF_DebugCommand(const char* d, int n, RF_DebugCommandCallback cback);
};
void RF_DebugConsoleHelpCommand(int argc, const char* argv[]);

#define DEBUG_BUFLEN 512
int socketListen(void *v);

class RF_DebugConsoleListener : public RF_Process
{
  public:
    static RF_DebugConsoleListener* instance;
    RF_DebugConsoleListener();
    virtual ~RF_DebugConsoleListener();

    virtual void Start();
    virtual void Update();
    void prepareSocket();
    void Listen();

    static std::string ip;
    static int rec_port;
    static int send_port;

    static void writeLine(std::string text);
    static void Log(std::string text);
    static void addCommand(std::string command, RF_DebugCommand* dc);
    void checkCommand(std::string command);

  private:
    static std::map<string, RF_DebugCommand*> commands;
    static std::vector<std::string> log;

    float seg {0.0};
    bool computing {false};

    UDPsocket rec_sock;
    UDPpacket *rec_packet;
    IPaddress send_addr;
    int numrecv;
    std::string command;

    SDL_Thread *listener;
    int signal {RF_Structs::S_AWAKE};

    void _writeLine(std::string text);

    friend void RF_DebugConsoleHelpCommand(int argc, const char* argv[]);
};
#endif //RF_DEBUGCONSOLE_H
