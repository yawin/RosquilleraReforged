/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef RF_STRUCTS_H
#define RF_STRUCTS_H

#include <ostream>
#include <sstream>
#include <string>
#include <vector>
using string = std::string;

namespace RF_Structs
{
  enum RF_KeyCode{
    _a,_b,_c,_d,_e,_f,_g,_h,_i,_j,_k,_l,_m,_n,_o,_p,_q,_r,_s,_t,_u,_v,_w,_x,_y,_z,
    _1,_2,_3,_4,_5,_6,_7,_8,_9,_0,
    _return,_esc,_backspace,_tab,_space, _minus, _equals, _leftbracket, _rightbracket, _backslash, _nonushash, _semicolon, _apostrophe, _grave, _comma, _period, _slash, _left_shift, _right_shift, _close_window,
    _left, _right, _up, _down,
    _FOO_KEY
  };

  enum RF_JKeyCode{
    _equis = 0, _circle = 1, _triangle = 2, _square = 3,
    _L1 = 4, _R1 = 5, _L2 = 6, _R2 = 7, _select = 8, _start = 9, _home = 10, _home2 = 11,
    _jpad_up = 13, _jpad_down = 14, _jpad_left = 15, _jpad_right = 16, _jpad_dir_left = 17, _jpad_dir_right = 18, _jpad_dir_up = 19, _jpad_dir_down = 20,
    _FOO_JKEY
  };

  enum RF_MouseButtonCode{
    _leftbutton, _rightbutton,
    _FOO_BUTTON
  };

  enum RF_Signal{
    S_AWAKE,
    S_AWAKE_TREE,
    S_KILL,
    S_KILL_TREE,
    S_KILL_CHILD,
    S_SLEEP,
    S_SLEEP_TREE
  };

  #define LERP(a,b,t) ((1-t)*a + t*b)

  template<typename T>
  struct Vector2{
    T x,y;
    Vector2(T x0=0, T y0=0){x=x0; y=y0;}
    Vector2(const Vector2& c){x=c.x; y=c.y;}

    bool operator==(const Vector2& c)
    {
      return (x == c.x) && (y == c.y);
    }

    bool operator!=(const Vector2& c)
    {
      return !((x == c.x) && (y == c.y));
    }

    Vector2 operator=(const Vector2& c)
    {
      if(this != &c)
      {
        this->x = c.x;
        this->y = c.y;
      }
      return *this;
    }

    Vector2 operator+(const Vector2& c)
    {
      return Vector2(x + c.x, y + c.y);
    }

    void operator+=(const Vector2& c)
    {
      x+=c.x;
      y+=c.y;
    }

    Vector2 operator-(const Vector2& c)
    {
      return Vector2(x - c.x, y - c.y);
    }

    void operator-=(const Vector2& c)
    {
      x-=c.x;
      y-=c.y;
    }

    Vector2 operator*(const Vector2& c)
    {
      return Vector2(x*c.x, y*c.y);
    }

    Vector2 operator*(const int& c)
    {
      return Vector2(x*c, y*c);
    }

    std::ostream& operator<<(std::ostream& o)
    {
      o << "{" << x << "," << y << "}";
      return o;
    }
  };

  template<typename T>
  std::ostream& operator<<(std::ostream& o, const Vector2<T>& obj)
  {
    o << "{" << obj.x << "," << obj.y << "}";
    return o;
  }

  template<typename P, typename S, typename R>
  struct Transform2D{
    Vector2<P> position;
    Vector2<S> scale;
    R rotation;

    std::ostream& operator<<(std::ostream& o)
    {
      o << "{p:" << position << ", s:" << scale << ", r:{" << rotation << "}}";
      return o;
    }
  };

  template<typename P, typename S, typename R>
  std::ostream& operator<<(std::ostream& o, const Transform2D<P,S,R>& t)
  {
    o << "{p:" << t.position << ", s:" << t.scale << ", r:{" << t.rotation << "}}";
    return o;
  }

  struct RF_Mouse
  {
    Vector2<int> position {0,0};
    bool Click[_FOO_BUTTON];
  };

  std::vector<string> splitLine(string str, const char* pattern);
};

class RF_Engine;
class RF_Process;
class RF_Window;
#endif // RF_STRUCTS_H
