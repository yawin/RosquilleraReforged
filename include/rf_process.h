/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef RF_PROCESS_H
#define RF_PROCESS_H

#define TO_RADIAN 0.0174444444445

#include "rf_structs.h"
#include "rf_window.h"
#include <SDL2/SDL.h>
#include <string>
#include <iostream>
#include <set>
using string = std::string;

extern const SDL_RendererFlip FLIP_NONE;
extern const SDL_RendererFlip FLIP_H;
extern const SDL_RendererFlip FLIP_V;

class RF_Process
{
  public:
    RF_Process(string _type = "null", RF_Window *_window = nullptr);
    virtual ~RF_Process();

    virtual void Start();
    virtual void Update();
    virtual void FixedUpdate();
    virtual void Draw();
    virtual void LateDraw();
    virtual void AfterDraw();
    virtual void Dispose();

    /**Propiedades******/
      string id {""}, father {""};
      unsigned int signal {RF_Structs::S_AWAKE};
      string type {""};
      SDL_Surface *graph {nullptr};
      RF_Structs::Transform2D<float, float, float> transform {{0.0f,0.0f},{1.0f,1.0f},0};
      int zLayer {-1};
      SDL_RendererFlip flipType {FLIP_NONE};
    /*******************/

    RF_Window *window {nullptr};

    bool operator<(const RF_Process* p) const;
    SDL_Rect getDimensions();
    RF_Structs::Vector2<int> checkPoint(RF_Structs::Vector2<int> p);
    RF_Structs::Vector2<int> checkPoint(int p_x, int p_y);

  private:
    /*
      Estas variables auxiliares se declaran fuera de las funciones
      para evitar su reserva y liberación constante del stack. Eso
      destrozaría la performance.

      Se emplea el prefijo "private_" para evitar reservar esos nombres
      y que no puedan emplearse en clases hijo.
    */
      float private_width, private_height, private_rotation, private_x, private_y, private_xa, private_ya, private_xb, private_yb;
      float private_vertices[4][2] { {-1, -1}, {1, -1}, {-1, 1}, {1, 1} };
};

#endif // RF_PROCESS_H
