/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef RF_ASSETMANAGER_H
#define RF_ASSETMANAGER_H

#include "rf_asset.h"
#include "rf_engine.h"
#include "rf_primitive.h"

#include <type_traits>
#include <memory>
#include <string>
using string = std::string;

namespace RF_AssetManager
{
  namespace Private
  {
    extern std::unordered_map<std::string, RF_AssetList*> packages;
  };

  void loadAssetPackage(std::string package);
  void unloadAssetPackage(std::string package);
  int packageSize(std::string package);
  int Size();

  bool isLoaded(std::string package, std::string id = "");

  template <typename T>
  typename T::element_type Get(std::string package, std::string id)
  {
    static_assert(std::is_base_of<RF_Asset, T>::value, "T must derive from RF_Asset");
    assert(isLoaded(package, id) == true);

    return Private::packages[package]->Get<T>(id);
  }

  void addResource(std::string extension, LoadFunc function);
};
#endif //RF_ASSETMANAGER_H
