/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef RF_ENGINE_H
#define RF_ENGINE_H

#include "rf_window.h"
#include "rf_process.h"
#include "rf_taskmanager.h"
#include "rf_input.h"
#include "rf_time.h"
#include "rf_debugconsole.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>

#include <sstream>
#include <assert.h>
#include <algorithm>
#include <string>
#include <unordered_map>
using string = std::string;

class RF_Engine
{
	public:
		static RF_Engine* instance;

		RF_Engine(bool debug=false);
		virtual ~RF_Engine();
		static void Run();

		//Windows
			static RF_Window* addWindow(std::string i_title = "", int i_width = 20, int i_height = 20, int i_posX = SDL_WINDOWPOS_CENTERED, int i_posY = SDL_WINDOWPOS_CENTERED, int i_windowMode = SDL_WINDOW_OPENGL, int i_rendererMode = SDL_RENDERER_ACCELERATED);
			//Getters y setters
	            static RF_Window* getWindow(int id);
	            static int getWindow(RF_Window *window);
	            static RF_Window* MainWindow();
	            static void MainWindow(int id);
	            static void MainWindow(RF_Window *_window);

	    static void closeWindow(int id);
	    static void closeWindow(RF_Window *_window);

		//Tasks
			template<class T = RF_Process>
			static std::string newTask(string father = "", RF_Window *_window = MainWindow())
			{
				assert(instance != nullptr);
				return RF_TaskManager::instance->newTask<T>(father, _window);
			}

			template <class T = RF_Process>
			static T* getTask(std::string id)
			{
			    static_assert(std::is_base_of<RF_Process, T>::value, "T must derive from RF_Process");
			    return reinterpret_cast<T*>(RF_TaskManager::instance->getTask(id));
			}

			static bool existsTask(std::string id);

		//Señales
			static void sendSignal(std::string taskID, int signal);
			static void sendSignal(RF_Process* task, int signal);
			static void sendSignalByType(std::string type, int signal);

		//FPS
			static const int Fps();
			static void Fps(int _fps);

		///////////////////////////////////////
			RF_Time Clock;
		///////////////////////////////////////

		template<typename T>
		static void Debug(T param)
		{
		    if(!isDebug){return;}

				std::stringstream text;
					text << "[" << SDL_GetTicks() << "]: " << param;

				RF_DebugConsoleListener::Log(string(text.str()));
		}

		static bool& Status(){return isRunning;}

		template<typename T>
		static void Start(bool debug = false)
		{
			new RF_Engine(debug);
			newTask<T>();

			do
			{
				Run();
			} while(Status());

			delete(instance);
		}

    private:
			float fps {0.1f/60.0f};
			float wait_to_draw {0.0};
			static bool isDebug, isRunning;
			int mainWindow {-1}, windowCount {0};
			std::unordered_map<int, RF_Window*> windowList;
};

#endif // RF_ENGINE_H
