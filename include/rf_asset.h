/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef RF_ASSET_H
#define RF_ASSET_H

#include "rf_engine.h"
#include "rf_primitive.h"
#include "utils/parser.h"

#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>

#include <type_traits>
#include <memory>
#include <dirent.h>
#include <fstream>
#include <unordered_map>
#include <string>
using string = std::string;

class RF_Asset
{
  public:
    RF_Asset(std::string name){id = name;}
    virtual ~RF_Asset(){}

    std::string id;

    template <typename T>
    typename T::element_type Get()
    {
      static_assert(std::is_base_of<RF_Asset, T>::value, "T must derive from RF_Asset");
      return reinterpret_cast<typename T::element_type>(GetSource());
    }

  private:
    virtual void* GetSource() = 0;
};

class RF_AudioClip : public RF_Asset
{
  public:
    RF_AudioClip(std::string name, Mix_Music* song);
    virtual ~RF_AudioClip();

    Mix_Music* clip {nullptr};

    void* GetSource();
    typedef Mix_Music* element_type;
};

class RF_FXClip : public RF_Asset
{
  public:
    RF_FXClip(std::string name, Mix_Chunk* fx);
    virtual ~RF_FXClip();

    Mix_Chunk* clip {nullptr};

    void* GetSource();
    typedef Mix_Chunk* element_type;
};

class RF_Gfx2D : public RF_Asset
{
  public:
    RF_Gfx2D(std::string name, SDL_Surface* gfx);
    virtual ~RF_Gfx2D();

    SDL_Surface* surface {nullptr};

    void* GetSource();
    typedef SDL_Surface* element_type;
};

class RF_Font : public RF_Asset
{
  public:
    RF_Font(std::string name, TTF_Font* ttf, string _path);
    virtual ~RF_Font();

    TTF_Font* font {nullptr};
    string path {""};

    void* GetSource();
    typedef TTF_Font* element_type;
};

typedef RF_Asset* (*LoadFunc)(std::string path, std::string Aid, std::string ext, Parser* parser);
RF_Asset* loadAudio(std::string path, std::string Aid, std::string ext, Parser* parser);
RF_Asset* loadGfx2D(std::string path, std::string Aid, std::string ext, Parser* parser);
RF_Asset* loadFont(std::string path, std::string Aid, std::string ext, Parser* parser);

class RF_AssetList
{
  public:
    RF_AssetList(std::string path);
    virtual ~RF_AssetList();

    std::string id {""};

    template <typename T>
    typename T::element_type Get(std::string id)
    {
      static_assert(std::is_base_of<RF_Asset, T>::value, "T must derive from RF_Asset");
      return assets[id]->Get<T>();
    }

    bool isLoaded(std::string id);
    int Size();

    static void addResource(std::string extension, LoadFunc function);

  private:
    std::unordered_map<std::string, RF_Asset*> assets;
    static std::unordered_map<std::string, LoadFunc> loadFunctions;
};

#endif //RF_ASSET_H
