#!/usr/bin/env python3

import socket, sys
from datetime import datetime, date, time

debugBox = None
W_PORT = 50002
R_PORT = 50003
ADDR = 'localhost'

s_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
r_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

def setDebugBox(dB):
    global debugBox
    debugBox = dB

def setAddr(adr, p):
    W_PORT = p
    ADDR = adr

def log(msg):
    global debugBox
    try:
        if debugBox:
            debugBox.texto += (str(msg) + "\n")
    except Exception as err:
        pass

    s.sendto(str(msg).encode("utf8", "backslashreplace"), (ADDR, W_PORT))

if __name__ == "__main__":
    if len( sys.argv ) > 2:
        W_PORT = sys.argv[1]

    s_adr = ('', W_PORT)
    s_sock.connect(s_adr)

    r_adr = ('255.255.255.255', R_PORT)
    r_sock.bind(r_adr)

    print("\n Debug console connected (localhost:" + str(W_PORT) + ")\n------------------------------------------------\n")
    while True:
        #data = r_sock.recv(1024);
        #print(data.decode("utf8", "backslashreplace"))
        try:
            com = input("$ > ");
            if(com == "!close"):
                break

            s_sock.sendto(str(com).encode("utf8", "backslashreplace"), (ADDR, W_PORT));

            d = ""
            while d != "DONE":
                data, address = r_sock.recvfrom(1024); #RECV

                d = data.decode("utf8", "backslashreplace")
                if(d != "DONE"):
                    print(data.decode("utf8", "backslashreplace"))
                else:
                    print("")

        except Exception as e:
            print("[{}] {} > {}".format(type(e).__name__, str(datetime.now()).split(".")[0], e))

    s_sock.close()
    r_sock.close()
